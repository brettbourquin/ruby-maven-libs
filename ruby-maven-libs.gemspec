# -*- mode:ruby -*-
require './lib/maven'
Gem::Specification.new do |s|
  s.name = 'ruby-maven-libs'
  s.version = Maven::VERSION

  s.authors = ["Christian Meier"]
  s.description = %q{maven distribution as gem - no ruby executables !} 
  s.summary = %q{maven distribution as gem}
  s.email = ["m.kristian@web.de"]

  s.extra_rdoc_files = Dir.glob("maven-home/*.txt") +
    Dir.glob( "maven-home/NOTICE*" ) + 
    Dir.glob( "maven-home/LICENSE*" ) + 
    Dir.glob( "README.md" )

  s.license = 'APL' 

  s.files = Dir.glob("maven-home/*.txt") +
            Dir.glob("maven-home/bin/*") +
            Dir.glob("maven-home/boot/*") +
            Dir.glob("maven-home/conf/**/*") +
            Dir.glob("maven-home/lib/**/*") +
            Dir.glob("lib/*.rb") +
            Dir.glob("*.md")
  
  s.homepage = %q{https://github.com/takari/ruby-maven-libs}
end

# vim: syntax=Ruby

